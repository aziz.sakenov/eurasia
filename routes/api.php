<?php

use App\Http\Controllers\EurasiaConsaltController;
use App\Http\Controllers\LibraryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/task/one',[EurasiaConsaltController::class,'taskOne']);
Route::get('/task/two/{way}',[EurasiaConsaltController::class,'taskTwo']);
Route::get('/task/three/{way}',[EurasiaConsaltController::class,'taskThree']);
Route::get('/task/four/{number}',[EurasiaConsaltController::class,'taskFour']);
Route::get('/add/book',[LibraryController::class,'addBook']);
