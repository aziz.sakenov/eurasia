<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EurasiaConsaltController extends Controller
{
    public const FIRST = 0;

    //Каким будет результат выполнения следующего куска кода:
    public function taskOne()
    {
        $a = "0";
        if(isset($a)){
            echo 0;
        }else{
            echo 1;
        }
        if(empty($a)){
            echo 0;
        }else{
            echo 1;
        }
        if($a){
            echo 0;
        }else{
            echo 1;
        }
    }
    //Ответ: 001

    //Задан ассоциативный массив на 10 элементов.
    // Напишите код для получения ключа первого элемента 3-мя разными способами.
    public function taskTwo(int $way)
    {
        $array = [
            'Январь'    => 22,
            'Февраль'   => 1,
            'Март'      => 2,
            'Апрель'    => 3,
            'Июнь'      => 4,
            'Июль'      => 5,
            'Август'    => 6,
            'Сентябрь'  => 7,
            'Октябрь'   => 8,
            'Ноябрь'    => 9
        ];

        if($way <= 0 || $way > 3 || count($array) <= 0){
            return response()->json(['message' => 'Not found'],404);
        }

        return match ($way){
            1 => ['Способ 1' => $this->firstKey($array)],
            2 => ['Способ 2' => array_keys($array)[self::FIRST]],
            3 => ['Способ 3' => array_key_first($array)],
        };
    }

    //Задан массив с числовыми ключами.
    //Напишите код для получения значения последнего элемента массива 3-мя разными способами.
    public function taskThree(int $way)
    {
        $array = [
            22 => 'Январь',
            11 => 'Февраль',
            32 => 'Март',
            3  => 'Апрель',
            54 => 'Июнь',
            5  => 'Июль',
            6  => 'Август',
            7  =>  'Сентябрь',
            8  => 'Октябрь',
            98  => 'Ноябрь'
        ];

        if($way <= 0 || $way > 3 || count($array) <= 0){
            return response()->json(['message' => 'Not found'],404);
        }

        return match ($way){
            1 => ['Способ 1' => $this->lastKey($array)],
            2 => ['Способ 2' => end($array)],
            3 => ['Способ 3' => $array[array_key_last($array)]],
        };
    }

    //Напишите функцию вычисления факториала числа.
    public function taskFour(int $number)
    {
        return response()->json(['Факториал числа '.$number => $this->factorial($number)],201);
    }

    //Напишите структуру базы данных для хранения информации о своей книжной
    // Библиотеке: основная информация названия книг и имена авторов.
    public function taskFive()
    {

    }

    public function firstKey($array)
    {
        foreach ($array as $key => $value){
            return $key;
        }
    }

    public function lastKey($array)
    {
        $result = null;
        foreach ($array as $value){
            $result = $value;
        }
        return $result;
    }

    public function factorial(int $number):int
    {
        if ($number == 0 || $number == 1) {
            return 1;
        } else {
            return $number * $this->factorial($number - 1);
        }
    }
}
