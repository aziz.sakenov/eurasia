<?php

namespace App\Http\Controllers;

use App\Http\Domain\UseCaseLibrary\CreateBooksUseCase;

class LibraryController extends Controller
{
    public function __construct(private CreateBooksUseCase $booksUseCase)
    {}

    public function addBook()
    {
        $book = $this->booksUseCase->run();
        return [
          'id' => $book->id->getId(),
          'name' => $book->name->getName()
        ];
    }
}
