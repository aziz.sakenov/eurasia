<?php

namespace App\Http\Domain\RepositoryInterface;

use App\Http\Domain\Entity\Books;

interface LibraryRepositoryInterface
{
    public function addBook(Books $books);
    public function removeBook(Books $books);
    public function getById(int $id);
}
