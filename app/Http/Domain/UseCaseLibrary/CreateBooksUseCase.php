<?php

namespace App\Http\Domain\UseCaseLibrary;

use App\Http\Domain\ContextRepository\Books\Id;
use App\Http\Domain\ContextRepository\Books\Name;
use App\Http\Domain\Entity\Books;
use App\Http\Domain\RepositoryInterface\LibraryRepositoryInterface;

class CreateBooksUseCase
{
    public function __construct(
        private LibraryRepositoryInterface $libraryRepository
    ){}

    public function run()
    {
        $book = new Books(
            new Id(1),
            new Name('Мартин Иден'),
        );
        $this->libraryRepository->addBook($book);
        return $book;
    }
}
