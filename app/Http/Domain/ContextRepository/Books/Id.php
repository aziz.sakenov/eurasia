<?php

namespace App\Http\Domain\ContextRepository\Books;

final class Id
{
    public function __construct(private int $id)
    {}

    public function getId():int
    {
        return $this->id;
    }
}
