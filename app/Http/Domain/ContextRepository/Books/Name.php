<?php

namespace App\Http\Domain\ContextRepository\Books;

final class Name
{
    public function __construct(private string $name)
    {}

    public function getName():string
    {
        return $this->name;
    }
}
