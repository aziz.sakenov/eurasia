<?php

namespace App\Http\Domain\Entity;

use App\Http\Domain\ContextRepository\Books\Id;
use App\Http\Domain\ContextRepository\Books\Name;

final class Books
{
    public function __construct(
        public Id   $id,
        public Name $name,
    )
    {}
    public function getId(): Id
    {
        return $this->id;
    }
    public function getName(): Name
    {
        return $this->name;
    }
}
